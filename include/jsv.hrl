%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 15.10.2021 20:24

-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

%% ====================================================================
%% Define modules and variables
%% ====================================================================

%% Application
-define(APP, jsv).
-define(ServiceName, "Jsv"). % prefix for output in logs
-define(SUPV, jsv_supv).

%% other apps
-define(APPBL, basiclib).

%% validation
-define(JV, jsv_validation).
-define(TypesEts, 'jsv_validation_types_ets').

%% Common
-define(U, jsv_utils).
-define(UErrMsg, jsv_utils_error_msg).

%% basiclib app
-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).

%% ====================================================================
%% Types
%% ====================================================================

-type json() :: boolean() | integer() | map() | list().

%% ====================================================================
%% Define Records
%% ====================================================================

-record(check_cb, {check_data :: function(),
                   exec_checkfun :: function(),
                   add_path :: function(),
                   clear_state :: function()
                  }).

%% check_state record
-record(check_s, {key :: undefined | binary(),
                  value :: term(), % the json part on the current check iteration
                  type :: undefined | binary(), % value type at the current check iteration
                  dsc :: map() | tuple() | list(), % descriptor type at the current check iteration
                  json :: json(), % source json for validation
                  callbacks :: #check_cb{},
                  path :: binary() | [binary()], % path key at the current check iteration
                  path_lidx=0 :: integer(), % idx for list element in path
                  types_state :: term(), % local state for type checking
                  errors=[] :: [map()],
                  warnings=[] :: [map()]
                 }).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {cfglib,erl}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?LOGFILE,{Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?LOGFILE,Text)).

-define(OUT(Text), ?BLlog:out(Text)).
-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level,?LOGFILE,{Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level,?LOGFILE,Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).

%% local
-define(OUTL(Fmt,Args), io:format("~s. "++Fmt++"~n",[?ServiceName]++Args)).
-define(OUTL(Text), io:format("~s. "++Text++"~n",[?ServiceName])).