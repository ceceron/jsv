%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 16.10.2021 23:27
%%% @doc

-module(jsv_validation_types_object).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([type_name/0,
         check_data_by_type/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

-define(TypeName, <<"object">>).
-define(MLogPrefix, ?MODULE).

%% debug out
%%-define(VOUT(Text,Args), ?OUTL(Text,Args)).
%%-define(VOUT(Text), ?OUTL(Text)).
-define(VOUT(Text,Args), ok).
-define(VOUT(Text), ok).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec type_name() -> TypeName::binary().
%% ---
type_name() -> ?TypeName.

%% ---
-spec check_data_by_type(CheckState::#check_s{}) -> ok | {ok, Warnings::[map()]} | {error, Errors::[map()]} | {error, Errors::[map()], Warnings::[map()]}.
%% ---
check_data_by_type(#check_s{type=?TypeName,value=V}=CS) when is_map(V) ->
    #check_s{key=K,dsc=Descriptor}=CS,
    %
    TypeCheckDefaults = [{<<"keys">>,#{}},{<<"values">>,#{}},
                         {<<"object_rule">>,undef},{<<"skip_unknown_keys">>,false}],
    [Keys,Values,ObjectRule,SkipUKeys] = ?BU:maps_get_default(TypeCheckDefaults,Descriptor),
    %
    AllowedBoolKeys = [<<"keys">>,<<"values">>,<<"object_rule">>,<<"skip_unknown_keys">>,<<"type">>],
    DescrX = maps:without(AllowedBoolKeys,Descriptor),
    if
        map_size(DescrX)/=0 ->
            Reason = ?UErrMsg:make_reason_by_text("invalid ~s metadata for key '~s'.",[?TypeName,K],CS),
            {error,Reason};
        map_size(Keys)/=0 orelse map_size(Values)/=0 ->
            check_data_object(CS#check_s{dsc={Keys,Values}});
        ObjectRule/=undef ->
            [Req,Opt] = ?BU:maps_get_default([{<<"req">>,#{}},{<<"opt">>,#{}}],ObjectRule),
            CS1 = CS#check_s{key=undefined,dsc=[{<<"req">>,Req},{<<"opt">>,Opt}],types_state=#{skip_unknown_keys => SkipUKeys}},
            check_data_by_object_rule(CS1);
        true -> ok
    end;
check_data_by_type(CS) ->
    #check_s{key=K,type=T}=CS,
    ?VOUT("check_data_by_type. Invalid value for type '~s' CS:~120p",[?TypeName,CS]),
    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value must be '~s'",[K,T],CS),
    {error,Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------
%% Check by keys & values
%% -----------

%% ---
check_data_object(CS) ->
    #check_s{dsc={KeysDescr,ValuesDescr},value=V,callbacks=#check_cb{add_path=AddPathFun},path=Path}=CS,
    FCheck = fun(Kx,Vx,CSx) ->
                     CSx1 =
                         case map_size(KeysDescr)/=0 of
                             true ->
                                 CSxx1 = check_data_object_one(CSx#check_s{value=Kx,dsc=KeysDescr,path=AddPathFun(Kx,Path)}),
                                 ?VOUT("object. E1:~120p; W1:~120p",[CSxx1#check_s.errors,CSxx1#check_s.warnings]),
                                 ?VOUT("object. dsc:~120p",[CSxx1#check_s.dsc]),
                                 ?VOUT("object. Value:~120p",[CSxx1#check_s.value]),
                                 CSxx1;
                             false -> CSx
                         end,
                     ?VOUT("---"),
                     CSx2 =
                         case map_size(ValuesDescr)/=0 of
                             true ->
                                 %PathItem = <<Kx/binary,".",Vx/binary>>,
                                 PathItem = Kx,
                                 CSxx2 = check_data_object_one(CSx1#check_s{value=Vx,dsc=ValuesDescr,path=AddPathFun(PathItem,Path)}),
                                 ?VOUT("object. E2:~120p; W2:~120p",[CSxx2#check_s.errors,CSxx2#check_s.warnings]),
                                 ?VOUT("object. dsc2:~120p",[CSxx2#check_s.dsc]),
                                 ?VOUT("object. Value2:~120p",[CSxx2#check_s.value]),
                                 CSxx2;
                             false -> CSx1
                         end,
                     ?VOUT("==="),
                     CSx2
             end,
    check_data_object_result(maps:fold(FCheck,CS,V)).

%% @private
check_data_object_one(CS) ->
    #check_s{callbacks=#check_cb{check_data=CheckDataFun,clear_state=ClearStateFun},errors=E,warnings=W}=CS,
    case CheckDataFun(ClearStateFun(CS)) of
        {ok, ResMap} ->
            Warns1 = maps:get(warnings,ResMap,[]),
            CS#check_s{warnings=Warns1++W};
        {error, ErrMap} ->
            Errors1 = maps:get(errors,ErrMap),
            Warns1 = maps:get(warnings,ErrMap,[]),
            CS#check_s{errors=Errors1++E, warnings=Warns1++W}
    end.

%% @private
check_data_object_result(#check_s{errors=[],warnings=[]}) -> ok;
check_data_object_result(#check_s{errors=[],warnings=W}) -> {ok,W};
check_data_object_result(#check_s{errors=E,warnings=[]}) -> {error,E};
check_data_object_result(#check_s{errors=E,warnings=W}) -> {error,E,W}.

%% -----------
%% Check object_rule
%% -----------

%% ---
check_data_by_object_rule(#check_s{dsc=[{<<"req">>,Req}|T]}=CS) when map_size(Req)==0 ->
    check_data_by_object_rule(CS#check_s{dsc=T});
check_data_by_object_rule(#check_s{dsc=[{<<"opt">>,Opt}|T]}=CS) when map_size(Opt)==0 ->
    check_data_by_object_rule(CS#check_s{dsc=T});
check_data_by_object_rule(#check_s{dsc=[],value=V,errors=[],warnings=[]}) when map_size(V)==0 -> ok;
check_data_by_object_rule(#check_s{dsc=[],value=V,errors=[],warnings=W}) when map_size(V)==0 -> {ok,W};
check_data_by_object_rule(#check_s{dsc=[],value=V,errors=E,warnings=[]}) when map_size(V)==0 -> {error,E};
check_data_by_object_rule(#check_s{dsc=[],value=V,errors=E,warnings=W}) when map_size(V)==0 -> {error,E,W};
check_data_by_object_rule(#check_s{dsc=[],value=V,types_state=TS}=CS)
  when map_size(V)/=0 andalso map_get(skip_unknown_keys,TS)==true ->
    check_data_by_object_rule(CS#check_s{value=#{}});
check_data_by_object_rule(#check_s{dsc=[],value=V}=CS) when map_size(V)/=0 ->
    #check_s{callbacks=#check_cb{add_path=AddPathFun},path=Path,errors=E,warnings=W}=CS,
    UnknownKeys = maps:keys(V),
    F = fun(UnknownKey,Acc) -> Acc ++ ?UErrMsg:make_reason_by_text("Found unknown key '~s'",[UnknownKey],CS#check_s{path=AddPathFun(UnknownKey,Path)}) end,
    Errs = lists:foldl(F,[],UnknownKeys),
    Errors = E ++ Errs,
    case W of
        [] -> {error,Errors};
        [_|_] -> {error,Errors,W}
    end;
%check_data_by_object_rule(#check_s{errors=E,warnings=[]}) when E/=[] -> {error,E};
%check_data_by_object_rule(#check_s{errors=E,warnings=W}) when E/=[] -> {error,E,W};
check_data_by_object_rule(#check_s{dsc=[{DescrType,DescrX}|T],value=V,errors=E,warnings=W}=CS)
  when DescrType== <<"req">>; DescrType== <<"opt">> ->
    #check_s{callbacks=#check_cb{clear_state=ClearStateFun},errors=E,warnings=W}=CS,
    ?VOUT("Object rule. DescrType:~120p,DescrX:~120p",[DescrType,DescrX]),
    ?VOUT("Object rule. E:~120p; W:~120p",[E,W]),
    DescrKeys = maps:keys(DescrX),
    CS1 =
        case check_data_by_object_rule_one(DescrKeys,DescrType,ClearStateFun(CS#check_s{dsc=DescrX})) of
            ok -> CS;
            {ok,Warnings} -> CS#check_s{warnings=Warnings++W};
            {error,Errors} -> CS#check_s{errors=Errors++E};
            {error,Errors,Warnings} -> CS#check_s{errors=Errors++E,warnings=Warnings++W}
        end,
    ?VOUT("Object rule. E1:~120p; W1:~120p",[CS1#check_s.errors,CS1#check_s.warnings]),
    ?VOUT("Object rule. dsc:~120p",[CS1#check_s.dsc]),
    ?VOUT("Object rule. Value:~120p",[CS1#check_s.value]),
    ?VOUT("Object rule. DescrKeys:~120p",[DescrKeys]),
    ?VOUT("Object rule. T:~120p",[T]),
    CS2 = CS1#check_s{dsc=T, value=maps:without(DescrKeys,V)},
    ?VOUT("Object rule. CS2:~120p",[CS2]),
    check_data_by_object_rule(CS2).

%% -------------------------------------
check_data_by_object_rule_one([],_,#check_s{errors=[],warnings=[]}) -> ok;
check_data_by_object_rule_one([],_,#check_s{errors=[],warnings=W}) -> {ok,W};
check_data_by_object_rule_one([],_,#check_s{errors=E,warnings=[]}) -> {error,E};
check_data_by_object_rule_one([],_,#check_s{errors=E,warnings=W}) -> {error,E,W};
%check_data_by_object_rule_one(_,_,#check_s{errors=E,warnings=[]}) when E/=[] -> {error,E};
%check_data_by_object_rule_one(_,_,#check_s{errors=E,warnings=W}) when E/=[] -> {error,E,W};
check_data_by_object_rule_one([DescrKey|T],DescrType,CS) ->
    #check_s{value=V,dsc=Descr,callbacks=CBs,path=P,errors=E,warnings=W}=CS,
    #check_cb{check_data=CheckDataFun,clear_state=ClearStateFun,add_path=AddPathFun}=CBs,
    case maps:get(DescrKey,V,undef) of
        undef when DescrType== <<"req">> ->
            {error,?UErrMsg:make_reason_by_text("missing required key '~s'",[DescrKey],CS)};
        undef when DescrType== <<"opt">> -> check_data_by_object_rule_one(T,DescrType,CS);
        Vx ->
            DescrX = maps:get(DescrKey,Descr),
            ?VOUT("check_data_by_object_rule_one. dscKey:~120p",[DescrKey]),
            ?VOUT("check_data_by_object_rule_one. Vx:~120p",[Vx]),
            ?VOUT("check_data_by_object_rule_one. dsc:~120p",[DescrX]),
            CS1 =
                case CheckDataFun(ClearStateFun(CS#check_s{key=DescrKey,value=Vx,dsc=DescrX,path=AddPathFun(DescrKey,P)})) of
                    {ok, ResMap} ->
                        Warns1 = maps:get(warnings,ResMap,[]),
                        CS#check_s{warnings=Warns1++W};
                    {error, ErrMap} ->
                        Errors1 = maps:get(errors,ErrMap),
                        Warns1 = maps:get(warnings,ErrMap,[]),
                        CS#check_s{errors=Errors1++E, warnings=Warns1++W}
                end,
            check_data_by_object_rule_one(T,DescrType,CS1)
    end.