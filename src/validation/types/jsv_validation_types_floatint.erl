%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 17.10.2021 21:38
%%% @doc

-module(jsv_validation_types_floatint).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([type_name/0,
         check_data_by_type/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

-define(TypeName, <<"floatint">>).
-define(MLogPrefix, ?MODULE).

%% debug out
%%-define(VOUT(Text,Args), ?OUT(Text,Args)).
%%-define(VOUT(Text), ?OUT(Text)).
-define(VOUT(Text,Args), ok).
-define(VOUT(Text), ok).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec type_name() -> TypeName::binary().
%% ---
type_name() -> ?TypeName.

%% ---
-spec check_data_by_type(CheckState::#check_s{}) -> ok | {ok, Warnings::[map()]} | {error, Errors::[map()]} | {error, Errors::[map()], Warnings::[map()]}.
%% ---
check_data_by_type(#check_s{type=?TypeName,value=V}=CS) when is_integer(V) ->
    check_data_by_type(CS#check_s{value=?BU:to_float(V)});
check_data_by_type(#check_s{type=?TypeName,value=V}=CS) when is_float(V) ->
    #check_s{key=K,dsc=Descriptor,callbacks=CBs}=CS,
    #check_cb{exec_checkfun=ExecCheckFun}=CBs,
    %
    [Min,Max,CheckF] = ?BU:maps_get_default([{<<"min">>,undef},{<<"max">>,undef},{<<"checkfun">>,undef}],Descriptor),
    %
    AllowedBoolKeys = [<<"min">>,<<"max">>,<<"checkfun">>,<<"type">>],
    DescrX = maps:without(AllowedBoolKeys,Descriptor),
    if
        map_size(DescrX)/=0 ->
            Reason = ?UErrMsg:make_reason_by_text("invalid ~s metadata for key '~s'.",[?TypeName,K],CS),
            {error,Reason};
        CheckF/=undef -> ExecCheckFun(CheckF,CS);
        true ->
            case {Min,Max} of
                {_,_} when is_float(Min) andalso is_float(Max) andalso V>=Min andalso V=<Max -> ok;
                {_,undef} when is_float(Min) andalso V>=Min -> ok;
                {undef,_} when is_float(Max) andalso V=<Max -> ok;
                {undef,undef} -> ok;
                _ ->
                    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value out of range",[K],CS),
                    {error,Reason}
            end
    end;
check_data_by_type(CS) ->
    #check_s{key=K,type=T}=CS,
    ?VOUT("check_data_by_type. Invalid value for type '~s' CS:~120p",[?TypeName,CS]),
    Reason = ?UErrMsg:make_reason_by_text("invalid '~s'. Value must be '~s'",[K,T],CS),
    {error,Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================