%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 16.10.2021 19:54
%%% @doc

-module(jsv_utils_error_msg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([make_reason_by_msg/2,
         msg/1, msg/2,
         make_reason_by_text/2, make_reason_by_text/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("jsv.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec make_reason_by_text(Text::string() | binary(), CS::#check_s{}) -> Reason::[map()].
%% ---
make_reason_by_text(Text, CS) ->
    Msg = msg(Text),
    make_reason_by_msg(Msg,CS).

%% ---
-spec make_reason_by_text(Text::string() | binary(), Args::[term()], CS::#check_s{}) -> Reason::[map()].
%% ---
make_reason_by_text(Text, Args, CS) ->
    Msg = msg(Text,Args),
    make_reason_by_msg(Msg,CS).

%% ---
-spec make_reason_by_msg(Msg::binary(), CS::#check_s{}) -> Reason::[map()].
%% ---
%% value must be list (errors accumulate)
%% ex. msg -> ?VU:msg("server ~s not defined in category structure", [SrvName])
make_reason_by_msg(Msg,#check_s{path=Path}=CS) when is_binary(Path) ->
    Item = resolve_item_bp(CS), % temporary decision
    [#{<<"msg">> => Msg,
       <<"path">> => Path,
       <<"item">> => Item}];
make_reason_by_msg(Msg,#check_s{path=[Path|_]=Paths}=CS) when is_binary(Path) ->
    Items = resolve_item_bp(CS), % temporary decision
    [#{<<"msg">> => Msg,
       <<"paths">> => Paths,
       <<"items">> => Items}].

%% ---
-spec msg(Text::string() | binary()) -> Msg::binary().
%% ---
msg(Text) ->
    ?BU:to_unicode_binary(Text).

%% ---
-spec msg(Text::string() | binary(), Args::[term()]) -> Msg::binary().
%% ---
msg(Text, Args) ->
    ?BU:strbin(Text,[?BU:to_unicode_binary(X) || X <- Args]).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
resolve_item_bp(#check_s{path=Path}) when Path== <<>>; Path== <<"/">> -> <<>>;
resolve_item_bp(#check_s{path=Path}=CS) when is_binary(Path) ->
    #check_s{json=Json}=CS,
    Path1 = prepare_path(Path),
    bp_get_item(Path1,Json);
resolve_item_bp(#check_s{path=Paths}=CS) when is_list(Paths) ->
    #check_s{json=Json}=CS,
    [bp_get_item(prepare_path(Px),Json) || Px <- Paths,Px/= <<>>,Px/= <<"/">>].

%% @private
prepare_path(Path) ->
    SplPath = binary:split(Path,<<"/">>,[global]),
    case ?BU:is_int_val(lists:last(SplPath)) of
        true -> Path;
        false when length(SplPath)>2 -> ?BU:join_binary(lists:droplast(SplPath),<<"/">>);
        false -> Path
    end.

%% ---
-spec bp_get_item(Path::binary(),Cfg::map()) -> Result::term().
%% ---
bp_get_item(Path,Cfg) ->
    bp_get_n_parent_item(0,Path,Cfg).

%%%% ---
%%-spec bp_get_parent_item(Path::binary(),Cfg::map()) -> Result::term().
%%%% ---
%%bp_get_parent_item(Path,Cfg) ->
%%    bp_get_n_parent_item(1,Path,Cfg).

%% ---
-spec bp_get_n_parent_item(N::integer(),Path::binary(),Cfg::map()) -> Result::term().
%% ---
bp_get_n_parent_item(N,Path,Cfg) ->
    ParsePath = bp_parse_path(Path),
    SplPath = lists:sublist(ParsePath,erlang:length(ParsePath)-N),
    bp_parse_cfg(SplPath,Cfg).

%% ---
bp_parse_path(Path) when is_binary(Path) ->
    F = fun(V) ->
        case ?BU:is_int_val(V) of
            true -> ?BU:to_int(V)+1;
            false -> V
        end
        end,
    lists:map(F,binary:split(Path,<<"/">>,[global,trim_all])).

%% ---
bp_parse_cfg([],Cfg) -> Cfg;
bp_parse_cfg([Key|T],Cfg) when is_binary(Key) ->
    bp_parse_cfg(T,maps:get(Key,Cfg));
bp_parse_cfg([Key|T],Cfg) when is_integer(Key) ->
    bp_parse_cfg(T,lists:nth(Key,Cfg)).